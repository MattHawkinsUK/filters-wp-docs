

1. Click on the **Edit** button;

	![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse hover the Web Part and click on the **[+]** button to add a new **Template of Filters**;

	![add_filter](../../images/modern/01.add_filter.gif)

3. Fill out the form that pops up. You can check what you need to do in each section on the [Template Settings](../../global/template);

4. After setting everything up, click on **Save** or **Save and Create Another** in case you want create more Tiles Templates with similar configuration. You can also preview the tile on the page before saving it, by clicking on the **Preview** button. 

<p class="alert alert-info"> Note this web part will only show items if you've items on Source List. The Filters templates will be displayed accordingly with the items that you've on the Source List. If you've 2 templates, it will build the content using the first template, then move to the next template. This will be repeated until steps until all the source list information is loaded.</p>