1. Click on the **Edit** button;

    ![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part click the **Manage Templates** icon;
3. The list of Filter Templates will appear. Click the **trash** icon to delete the Filter Template;
4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Template Filter will be removed.

    ![delete_filter](../../images/classic/03.delete_filter.png)