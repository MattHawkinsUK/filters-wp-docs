![frontandback](../images/classic/20.frontandback.png)

Depending on the type of the tile you choose, you can have a front tile and a back tile. 

<p class=" alert alert-success">The front tile is displayed by default and the back tile is displayed when the cursor hovers the tile or every few seconds.</p>

### Tile Background

Choose a **Color** or **Image** for the tile background.

For more information on how to use:
    
- The color picker, refer to <a href="../format/#color" target="_blank">this section</a>.
- The image picker, refer to <a href="../image" target="_blank">this section</a>.

If you intend you can map a column from the list you've connected on **Data Source list**. To use that column as **Image**, you need to place the column's internal name in brackets: <b>{{FIELD_NAME}}</b>.

Here is what you need to do to check your column name: 

1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...Field=..."**.  
5. Copy the internal name;
6. Now paste the name on the text box;

![internalcolumnname](../images/classic/23.internalcolumnname.png) 

___
### Caption Positions

Switch between the **TOP**, **CENTER** and **BOTTOM** tab to add the content to the top of tile, the center or the bottom of the tile. 

___
### Content

Type the text you want for your tile. You can use the format buttons to define headings, alignment, weight, colors and add icons to the text. or more information about the controls, check the [next link](./format.md)

You will need to use Markdown to add text to your tile. If you don't know much about Markdown read more about it on our <a href="https://support.bindtuning.com/hc/en-us/articles/213784643" target="_blank">Markdown syntax in BindTuning</a> article.

If you intend you can map a column from the list you've connected on **Data Source list**. To use that column as **Text**, you need to place the column's internal name in brackets: <b>{{TEXT}}</b>.

Here is what you need to do to check your column name: 

1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...Field=..."**.  
5. Copy the internal name;
6. Now paste the name on the text box;

![internalcolumnname](../images/classic/23.internalcolumnname.png) 

___
### Caption Background

Defines a background for your caption. Tiles can have 3 captions, one at the **TOP**, one at **CENTER** and another at the **BOTTOM** of the tile - depending on where you add your content. You can check the position where the **Caption Background** is applied by consulting the **Caption Position** property.



To enrich more the Filter template, you can configure the other sections that we explain below: 

- [Temmplate Settings](./template)
- [Setup a Link](./link)
- [Animations Settings](./animation)

After setting everything up, click on **Save** or **Save and Create Another** in case you want create more Tiles Templates with similar configuration. You can also preview the tile on the page before saving it, by clicking on the **Preview** button.