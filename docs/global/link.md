### Setup a Link

![browse_image](../images/classic/21.link.png)

Here you can set the link for your Link tile. It will open every time an user clicks on the tile. 

___
### Clickable Link

You can set the link for your Template Filter. It will open every time an user clicks on the tile. If you don't need a link, you can leave this section empty. 

If you intend you can map a column from the list you've connected on **Data Source list**. To use that column as **link**, you need to place the column's internal name in brackets: <b>{{URL}}</b>.

Here is what you need to do to check your column name: 

1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...Field=..."**.  
5. Copy the internal name;
6. Now paste the name on the text box;

![internalcolumnname](../images/classic/23.internalcolumnname.png) 
___
### Open Site In

You can also decide whether that link when ckicked will do:

- Same Window - Open the link on the same window/tab. 
- New Window - Open the link on the new window/tab. 
- Modal - Open the link inside a modal.

<p class="alert alert-info">Links pointing to external sources might be blocked when using the modal, since the content is displayed inside an iframe.</p>
<p class="alert alert-info">Links pointing to unsecure connections (using http:// instead of https://) will be blocked by SharePoint Online when using the modal.</p>

To enrich more the Filter template, you need to configure the other sections that we explain below: 

- [Temmplate Settings](./template)
- [Front Tiles and Back Tiles settings](./frontback)
- [Animations Settings](./animation)

After setting everything up, click on **Save** or **Save and Create Another** in case you want create more Tiles Templates with similar configuration. You can also preview the tile on the page before saving it, by clicking on the **Preview** button.