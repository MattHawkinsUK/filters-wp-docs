![Template Settings](../images/classic/18.template_settings.png)

### Template Name

This setting should contain a short name that describes the template you’re creating. This is used to identify the template in the list for further usage.

___
### Template Order

Set the order in which tiles appear. Tiles with lower values appear first; tiles with higher values appear last.

Tiles with the same value are ordered based on date of creation (older tiles first).

_____
### Template Status

You can make a template active or inactive by checking and unchecking this option.


To enrich more the Filter template, you can configure the other sections that we explain below: 

- [Setup a Link](./link)
- [Front Tiles and Back Tiles settings](./frontback)
- [Animations Settings](./animation)


After setting everything up, click on **Save** or **Save and Create Another** in case you want create more Tiles Templates with similar configuration. You can also preview the tile on the page before saving it, by clicking on the **Preview** button.