![animationsettings](../images/classic/22.animationsettings.png)

### Flip Animation

When the tile supports a front and back tile, you can set the animation you want your tile to show when switching between the front and back tile.

<p class="alert alert-success">When <strong>none</strong> is selected, the back tile will never appear.</p>

___
### Animation Interval 

Here you can define the seconds of the interval between the flip animations.

If the interval is **0**, the tile will only flip every time you place the cursor on the tile. If you set any other value, the tile will animate, revealing the back side or the next slide, every couple of seconds. 

<p class="alert alert-success">If you don't enter any value, the tile will animate every 5 seconds.</p

To enrich more the Filter template, you can configure the other sections that we explain below: 

- [Temmplate Settings](./template)
- [Setup a Link](./link)
- [Front Tiles and Back Tiles settings](./frontback)

After setting everything up, click on **Save** or **Save and Create Another** in case you want create more Tiles Templates with similar configuration. You can also preview the tile on the page before saving it, by clicking on the **Preview** button.