![06.options](../images/modern/13.filtering.png)
___
### Filters 

Here you define which columns of your document library will be used as filtering options. Each selected filter will be displayed as a dropdown element, with filter values.


The following example shows 2 filters: Topic and Department (therefore 2 mapped columns):

|**Configuration**|**Result**|
|-|-|
|![dropdowns_filters.png](../images/classic/30.dropdowns_internal.png)|![dropdowns_filters.png](../images/classic/29.dropdowns_filters.png)|

If no filters are defined, no dropdowns/filters will appear on the web part.<br />
To use a column as a filter, you need to place it's **internal name** into the field.<br />
To show a custom **display name** on the dropdown, place your text inside square brackets<br />
To have a **default value** selected, add two colons after the interal or display name followed by the selected value.

**e.g.: Dprt[Department]::HR, Topic::Web**

To use that column as Filter, you need to place the column's internal name. Here is what you need to do to check your column name: 

1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...Field=..."**.  
5. Copy the internal name;
6. Now paste the name on the text box;

![internalcolumnname](../images/classic/23.internalcolumnname.png) 

____
### Filters Layout

Here you can define tha layout that your filters will have. You can choose between **Dropdowns** or **Buttons**.

____
### Open Filters On

If you have chosen **Dropdowns** in the previous option, here you are able to choose how to open the dropdown, on **Click** or on **Hover**.