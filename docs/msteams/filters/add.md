1. Open the Team on **Teams panel** that you intend to add the Filter Template; 
2. Click on the **Settings** button. 

	![settings_delete.png](../../images/msteams/setting_edit.png)

2. Click on the **[+]** button to add a new Template;
3. Fill out the form that pops up. You can check out what you need to do in each setting in the <a href="../../global/template">Template Settings</a> section of this User Guide;
4. After setting everything up, click on the **Preview** button if you want to see how everything looks on the page, or click on **Save** or **Save and create another**.

	![add_new_template_teams.gif](../../images/msteams/add_new_template_teams.gif)

<p class="alert alert-success">Clicking <b>Save and create another</b> will keep the form open, so you can add more templates to your page without closing the form.</p>
