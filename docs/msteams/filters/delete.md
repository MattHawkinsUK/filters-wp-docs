1. Open the Team on **Teams panel** that you intend to delete the Filter Template; 
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/setting_edit.png)

3. Click on the **Manage Templates** icon;

	![Manage](../../images/msteams/manage_templates.png)

4. The list of templates will appear. Click the **trash can** icon to delete the template that you want to remove;

	![Delete](../../images/classic/03.delete_filter.png)

5. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the alert will be removed.