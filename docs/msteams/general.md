![options](../images/modern/07.options.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Create New BT Filters Template List](./createlist)
- [List Settings](./list)
- [Filtering Options](./filter)
- [Grid Settings](./grid)
- [Web Part Appearance](./appearance)
- [Advanced Options](./advanced)
- [Performance](./performance)
- [Web Part Messages](./message)