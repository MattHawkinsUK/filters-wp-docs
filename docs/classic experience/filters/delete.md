1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. On the web part sidebar and click the **Manage Templates** icon;
3. The list of Filter Templates will appear. Click the **trash** icon to delete the Filter Template;
4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the template will be removed.	

    ![delete_filter](../../images/classic/03.delete_filter.png)