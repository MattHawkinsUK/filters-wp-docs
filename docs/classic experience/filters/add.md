<p class="alert alert-info">You're adding the Filters template to the Web Part that will be based on the template number display on some of the items that you've on the Source List. Note this web part will only show items if you've items on Source List.</p>

1. Open the page where you've added the web part; 
2. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
3. Click on the ➕ (plus) icon to add a new Template of Filters;

	![add_filter](../../images/classic/04.add_filter.gif)

4. Fill out the form that pops up. You can check what you need to do in each section on the [Template Settings](../../global/template);

5. After setting everything up, click on **Save** or **Save and Create Another** in case you want create more Tiles Templates with similar configuration. You can also preview the tile on the page before saving it, by clicking on the **Preview** button. 

	![save](../../images/classic/16.save.png)


<p class="alert alert-info"> Note this web part will only show items if you've items on Source List. The Filters templates will be displayed accordingly with the items that you've on the Source List. If you've 2 templates, it will build the content using the first template, then move to the next template. This will be repeated until steps until all the source list information is loaded.</p>